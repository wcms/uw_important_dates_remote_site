<?php

/**
 * @file
 * API Functions for Important Dates Remote Site.
 */

/**
 * Helper function.
 *
 * Get the request from the central site.
 */
function _uw_important_dates_remote_site_get_request($remote_url, $query_parameters = NULL) {

  // Get the central site server from settings.
  $central_site_server = variable_get('uw_important_dates_central_site_server');

  // Get the central site URL from settings.
  $central_site_url = variable_get('uw_important_dates_central_site');

  // Get the API version number from settings.
  $api_version = variable_get('uw_important_dates_api_version');

  if (!$central_site_server || !$central_site_url || !$api_version) {
    drupal_set_message('Important Dates API not configured.', 'warning', FALSE);
    return;
  }

  // Set the URL based on the settings.
  $url = 'https://' . $central_site_server . '/' . $central_site_url . '/api/v' . $api_version . '/' . $remote_url;

  // Set the URL with the parameters.
  $url = url($url, array('query' => $query_parameters));

  // Check if we should ignore certificates.
  if (variable_get('uw_important_dates_ignore_certificates')) {
    $context = stream_context_create(array(
      'ssl' => array(
        'verify_peer' => FALSE,
        'verify_depth' => 5,
        'allow_self_signed' => TRUE,
        'verify_peer_name' => FALSE,
      ),
    ));
    $options['context'] = $context;
  }
  else {
    $options = array();
  }

  // Do the request.
  $request = drupal_http_request($url, $options);

  if (isset($request->error)) {
    $message = 'Important Dates API failure (' . REQUEST_TIME . ')';
    watchdog('uw_important_dates_remote_site', $message . ': ' . $request->error);
    drupal_set_message($message . '.', 'warning');
  }

  // Get the JSON data.
  $json_response = isset($request->data) ? drupal_json_decode($request->data) : NULL;

  return $json_response;
}

/**
 * Helper function.
 *
 * Get the drupal http request.
 *
 * @param string $taxonomy_vocabulary
 *   The taxonomy vocabulary.
 *
 * @return array
 *   Taxonomy terms
 */
function _uw_important_dates_remote_site_get_taxonomy_from_central_site($taxonomy_vocabulary) {

  // RT#1017499: academic term taxonomy name has an s on the end, so adding it here.
  if ($taxonomy_vocabulary == 'important_dates_academic_term') {
    $taxonomy_vocabulary .= 's';
  }

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request($taxonomy_vocabulary);

  // If there is a problem with the json, simply return.
  if (!is_array($json_response)) {
    return [];
  }

  // Define array.
  $terms = [];

  // Set through each json request and get the response data.
  foreach ($json_response as $response_data) {

    if (isset($response_data) && is_array($response_data)) {
      // Step through each response and get the audience.
      foreach ($response_data as $data) {
        if (isset($data['label'])) {
          if (isset($data['weight'])) {
            $terms[$data['weight']] = array('id' => $data['id'], 'term' => $data['label']);
          }
          else {
            $terms[$data['id']] = $data['label'];
          }
        }
      }
    }
  }

  return $terms;
}

/**
 * Helper function.
 *
 * Get the number of calendar entries.
 */
function _uw_important_dates_remote_site_get_calendar_count($operator, $query_parameters) {

  // Set the operator to compare the date with.
  $query_parameters['operator'] = $operator;

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request('important_dates_calendar_count', $query_parameters);
  if (!$json_response) {
    return;
  }

  // If there is a response, process it.
  if (isset($json_response) && $json_response !== NULL) {

    // Step through each JSON request and process data.
    foreach ($json_response as $response_data) {

      // If there is data to process, continue.
      if (isset($response_data) && is_array($response_data)) {

        // Step through each data and get the view html.
        foreach ($response_data as $data) {

          return $data['count'];
        }
      }
    }
  }
}
