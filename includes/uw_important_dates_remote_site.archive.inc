<?php

/**
 * @file
 * Functions relating to archives.
 */

/**
 * Helper function.
 *
 * Page callback for archive entry.
 */
function _uw_important_dates_remote_site_archive_entry() {

  // Get the parameters from the URL.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Get the page from the entry function.
  $page = _uw_important_dates_remote_site_entry($query_parameters);

  // Return the page array.
  return $page;
}

/**
 * If variable uw_important_dates_archive_exclude is set, add parameter.
 *
 * Clean-up the value of variable uw_important_dates_archive_exclude to make an
 * array of integers representing excluded date types. Add this as the 'exclude'
 * parameter to $query_parameters.
 *
 * @param array $query_parameters
 *   An array of query parameters to act on.
 */
function _uw_important_dates_add_archive_exclude(array &$query_parameters) {
  $uw_important_dates_archive_exclude = variable_get('uw_important_dates_archive_exclude');
  if ($uw_important_dates_archive_exclude) {
    $uw_important_dates_archive_exclude = array_filter(array_map('intval', array_values($uw_important_dates_archive_exclude)));
    if ($uw_important_dates_archive_exclude) {
      $query_parameters['exclude'] = json_encode($uw_important_dates_archive_exclude);
    }
  }
}

/**
 * Helper function.
 *
 * Page callback for archive by year and term.
 */
function _uw_important_dates_remote_site_archive_by_year_and_term($form, &$form_state) {

  // Variable to store whether valid archive.
  $valid_archive = FALSE;

  // Get the arguments from the URL.
  $args = arg();

  // If there are more than 4 arguments, return 404.
  if (count($args) > 4) {
    return drupal_not_found();
  }

  // Get the number of arguments.
  $arg_count = count($args);

  // Get the academic year.
  $query_parameters['academic_year'] = _uw_important_dates_remote_site_get_tid_by_term_name('important_dates_academic_year_by_name', 'academic_year', array('academic_year' => $args[$arg_count - 2]));

  // Get the academic term.
  $query_parameters['academic_term'] = _uw_important_dates_remote_site_get_tid_by_term_name('important_dates_academic_term_by_name', 'academic_term', array('academic_term' => $args[$arg_count - 1]));

  // If there is an academic year and term, process them.
  if (isset($query_parameters['academic_year']) && isset($query_parameters['academic_term'])) {

    // The current academic year from settings.
    $current_academic_year = variable_get('uw_important_dates_current_academic_year');

    // Get the current academic term from API.
    $current_academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $current_academic_year);

    // Set the current academic year.
    $current_academic_year = $current_academic_year['data'][0]['label'];

    // Get the academic year via API.
    $academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $query_parameters['academic_year']);

    // Set acacemic year.
    $academic_year = $academic_year['data'][0]['label'];

    // If the academic year provided is less than current, valid archive.
    if ($academic_year < $current_academic_year) {
      $valid_archive = TRUE;
    }

  }

  // Add the excluded types of dates to the query parameters.
  _uw_important_dates_add_archive_exclude($query_parameters);

  // Ensure that on archive all dates are captured.
  $query_parameters['date'] = 'Any';

  // If there is a valid archive, process it, if not set message.
  if ($valid_archive) {
    $form = _uw_important_dates_remote_site_list($form, $form_state, $query_parameters);
  }
  else {
    drupal_not_found();
  }

  return $form;
}

/**
 * Helper function.
 *
 * Page callback for archive for year only.
 */
function _uw_important_dates_remote_site_archive_by_year($form, &$form_state) {

  // Variable to tell us if this is a valid archive.
  $valid_archive = FALSE;

  // Get the arguments from the URL.
  $args = arg();

  // Get the number of arguments.
  $arg_count = count($args);

  // Set the academic year parameter based on arguments.
  $query_parameters['academic_year'] = $args[$arg_count - 1];

  $query_parameters['academic_year'] = _uw_important_dates_remote_site_get_tid_by_term_name('important_dates_academic_year_by_name', 'academic_year', $query_parameters);

  if (isset($query_parameters['academic_year'])) {

    // The current academic year from settings.
    $current_academic_year = variable_get('uw_important_dates_current_academic_year');

    // Get the current academic term from API.
    $current_academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $current_academic_year);

    // Set the current academic year.
    $current_academic_year = $current_academic_year['data'][0]['label'];

    // Get the academic year via API.
    $academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $query_parameters['academic_year']);

    // Set acacemic year.
    $academic_year = $academic_year['data'][0]['label'];

    if ($academic_year < $current_academic_year) {
      $valid_archive = TRUE;
    }
  }

  _uw_important_dates_add_archive_exclude($query_parameters);

  // Ensure that we add the Any date.
  $query_parameters['date'] = 'Any';

  // RT#1034476.
  // Ensuring that the page gets added as a parameter if it exists.
  $params = drupal_get_query_parameters();

  if (isset($params['page'])) {
    $query_parameters['page'] = (int) $params['page'];
  }

  // If there is a valid archive, process it, if not set message.
  if ($valid_archive) {
    $form = _uw_important_dates_remote_site_list($form, $form_state, $query_parameters);
  }
  else {
    drupal_not_found();
  }

  // Return the form.
  return $form;
}

/**
 * Helper function.
 *
 * Get the taxonomy tid using the term name.
 */
function _uw_important_dates_remote_site_get_tid_by_term_name($api_name, $term_name, $query_parameters) {

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request($api_name, $query_parameters);

  // If there is a tid, return it, if not return NULL.
  if (isset($json_response['data'][0][$term_name])) {

    // Return the tid.
    return $json_response['data'][0][$term_name];
  }
  else {
    return;
  }
}

/**
 * Helper function.
 *
 * Create a link to the list view.
 */
function _uw_important_dates_remote_site_create_archive_link($form, &$form_state) {

  // Get the query parameters.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Wrapper for link to important dates settings.
  $form['links'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-important-dates-links')),
  );

  // Link back to important dates settings.
  $form['links']['link'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Important dates settings'), 'admin/config/system/uw_important_dates_remote_site_settings', array('attributes' => array('class' => array('uw-important-dates-links-link')))),
  );

  // Wrapper for filters.
  $form['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-create-list-link-filters')),
  );

  // Get the academic year and add the Any option.
  $academic_years = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_academic_year');

  // The current academic year from settings.
  $current_academic_year = variable_get('uw_important_dates_current_academic_year');

  // Get the current academic term from API.
  $current_academic_year_term = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $current_academic_year);

  // Set the current academic year.
  $current_academic_year = $current_academic_year_term['data'][0]['label'];

  // Add the start and end date to academic year.
  foreach ($academic_years as $key => $academic_year) {

    // Limit the academic years to only those in the past.
    if ($academic_year < $current_academic_year) {

      // Separate the years.
      $years = explode(' - ', $academic_year);

      // Set the options for the academic year.
      $academic_years_options[$key] = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1];
    }
  }

  // The academic year filter drop down.
  $form['filters']['academic_year'] = array(
    '#type' => 'select',
    '#title' => 'Academic year',
    '#options' => $academic_years_options,
    '#default_value' => isset($query_parameters['academic_year']) ? $query_parameters['academic_year'] : $academic_years_setting,
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-year')),
  );

  // Get the academic terms, that are not sorted, but contain weight.
  $academic_terms_raw = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_academic_terms');

  // Sort the terms.
  ksort($academic_terms_raw);

  // Step through and get the academic term array, used as options.
  foreach ($academic_terms_raw as $academic_term_raw) {
    $academic_terms[$academic_term_raw['id']] = $academic_term_raw['term'];
  }

  // The academic term filter drop down.
  $form['filters']['academic_term'] = array(
    '#type' => 'select',
    '#title' => 'Academic term',
    '#options' => $academic_terms,
    '#empty_option' => '- Any -',
    '#default_value' => isset($query_parameters['academic_term']) ? $query_parameters['academic_term'] : '',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-term')),
  );

  // Submit button.
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Create',
  );

  // Add submit handler.
  $form['#submit'][] = '_uw_important_dates_remote_site_create_archive_link_submit';

  return $form;
}

/**
 * Helper function.
 *
 * Submit handler for create list link.
 */
function _uw_important_dates_remote_site_create_archive_link_submit($form, &$form_state) {

  // Variable to store parameters.
  $query_parameters = array();

  // URL to be used.
  $archive_url = 'important-dates/archive/';

  // If the academic year has a value, add to query parameters.
  if (isset($form_state['values']['academic_year']) && $form_state['values']['academic_year'] !== '') {

    // The URL to be used to contact the API.
    $api_url = 'important_dates_academic_year/' . $form_state['values']['academic_year'];

    // Get the JSON data.
    $json_response = _uw_important_dates_remote_site_get_request($api_url, $query_parameters);

    // If there is academic year, get the name without the spaces.
    if (isset($json_response['data'][0]['label'])) {

      // Set the academic year name.
      $academic_year_name = str_replace(' ', '', $json_response['data'][0]['label']);

      // Add academic year to the URL.
      $archive_url .= $academic_year_name;

      // Add the query parameters for the create archive link form.
      $query_parameters['academic_year'] = $form_state['values']['academic_year'];
    }
  }

  // If the academic term has a value, add to query parameters.
  if (isset($form_state['values']['academic_term']) && $form_state['values']['academic_term'] !== '') {

    // The URL to be used to contact the API.
    $api_url = 'important_dates_academic_terms/' . $form_state['values']['academic_term'];

    // Get the JSON data.
    $json_response = _uw_important_dates_remote_site_get_request($api_url, $query_parameters);

    // If there is academic year, get the name without the spaces.
    if (isset($json_response['data'][0]['label'])) {

      // Set the academic year name.
      $academic_term_name = str_replace(' ', '', $json_response['data'][0]['label']);

      // Add academic year to the URL.
      $archive_url .= '/' . $academic_term_name;

      // Add the query parameters for the create archive link form.
      $query_parameters['academic_term'] = $form_state['values']['academic_term'];
    }
  }

  // Generate the url with the parameters.
  $url = url($archive_url, array('absolute' => TRUE));

  // Go to the URL with the parameters.
  drupal_set_message($url, 'status');

  // Go to the settings page with parameters.
  drupal_goto('admin/config/system/uw_important_dates_create_archive_link', array('query' => $query_parameters));
}
