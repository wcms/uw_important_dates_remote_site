<?php

/**
 * @file
 * Code for the calendar for Important Dates Remote site feature.
 */

/**
 * Helper function.
 *
 * Returns form render array for important dates calendar view.
 */
function _uw_important_dates_remote_site_calendar() {

  global $_uw_important_dates_remote_site_num_of_entries;

  // Get the parameters from the URL.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Track if the month view.
  $month_view = TRUE;

  // Check if we have a day view and if so turn off month view.
  if (isset($query_parameters['navigate']) && preg_match("/[0-9]{4}-[0-9]{2}-[0-9]{1,2}/ms", $query_parameters['navigate'])) {
    $month_view = FALSE;
  }

  // If there is a reset, go to calender without any parameters.
  if (isset($query_parameters['op']) && $query_parameters['op'] == 'Reset') {

    // Go to the calendar with the no parameters.
    drupal_goto('important-dates/calendar');
  }

  // If we have a month a date, set the parameter navigate and remove year and
  // month. We have to do this, because the view does not like us using month
  // and year in the query parameters.
  if (isset($query_parameters['year']) && isset($query_parameters['month'])) {
    $query_parameters['navigate'] = $query_parameters['year'] . '-' . $query_parameters['month'];
    unset($query_parameters['year']);
    unset($query_parameters['month']);
  }

  // If there is no type set, set it to All, required by the view.
  if (isset($query_parameters['type']) && empty($query_parameters['type'])) {
    $query_parameters['type'] = 'All';
  }

  // If there is no audience set, set it to All, required by the view.
  if (isset($query_parameters['audience']) && $query_parameters['audience'] == '') {
    $query_parameters['audience'] = 'All';
  }

  // Get the sites that are restricted on the calendar from settings.
  $sites = variable_get('uw_important_dates_sites', []);

  // If there are site restrictions, add them to the parameters.
  foreach ($sites as $site) {
    $query_parameters['sites'][] = $site;
  }

  // If we have entries, get the calendar.
  if ($_uw_important_dates_remote_site_num_of_entries) {

    // Get the JSON data.
    $json_response = _uw_important_dates_remote_site_get_request('important_dates_remote_calendar', $query_parameters);
    if (!$json_response) {
      return;
    }

    // Step through each JSON request and process data.
    foreach ($json_response as $response_data) {

      // If there is data that is an array, process it.
      if (is_array($response_data)) {
        // Step through each data and get the view html.
        foreach ($response_data as $data) {

          // If there is view html, store it.
          if (isset($data['view_html'])) {
            $html = $data['view_html'];
          }
        }
      }
    }

    // Unsetting the sites, so they don't get including in the rest of the links.
    if (isset($query_parameters['sites'])) {
      unset($query_parameters['sites']);
    }

    // Get the pattern to get the calendar HTML from the response.
    if ($month_view) {
      $pattern = '/<div class="view-content">.*?<script>.*?<\/script>.*?<\/div>/ms';
    }
    else {
      $pattern = '/<div class="view-content">.*?(<\/div>\s*){4}/ms';
    }

    if (isset($html)) {
      // Get the calendar HTML from the response.
      preg_match($pattern, $html, $matches);

      // Set the calendar HTML based on the matches or no items.
      $calendar_html = isset($matches[0]) ? $matches[0] : '';

      if ($month_view) {
        // Get the variables for the central site.
        $central_server = variable_get('uw_important_dates_central_site_server');
        $central_site = variable_get('uw_important_dates_central_site');

        // Get the variables for the remote site.
        $remote_server = variable_get('uw_important_dates_remote_site_server');
        $remote_site = variable_get('uw_important_dates_remote_site');

        // The pattern for the day view link.
        $pattern = '/https:\/\/' . $central_server . '\/' . $central_site . '\/.*?day.*?([0-9]{4}-[0-9]{1,2}-[0-9]{1,2}).*?"/';

        // Get all teh matches for the day view link.
        preg_match_all($pattern, $calendar_html, $matches);

        // Set variables to use the links and the dates.
        $links = $matches[0];
        $dates = $matches[1];

        // Step through each link and replace with new link and parameters.
        foreach ($links as $key => $link) {

          // Use date_parameters to mimic query_parameters.
          $date_parameters = $query_parameters;

          // Add in the navigate to the query parameters.
          $date_parameters['navigate'] = date('Y-m-d', strtotime($dates[$key]));

          // Setup the replace link with the date_parameters.
          $replace = url('important-dates/calendar', array('query' => $date_parameters)) . '"';

          // Perform the replacement.
          $calendar_html = str_replace($link, $replace, $calendar_html);
        }

        // The pattern to find in the HTML to replace the day view
        // of the calendar.
        $pattern = '/\/important-dates\/calendar\/.*?\/[0-9]{4}-[0-9]{2}-[0-9]{2}/ms';

        // Get all the matches.
        preg_match_all($pattern, $calendar_html, $matches);

        // Step through each match and replace with parameters.
        foreach ($matches[0] as $match) {

          // Get the date from the matches.
          $date = explode('/', $match);

          // The day will be the last element in the array.
          $day = end($date);

          // Add the date to the navigate parameter.
          $query_parameters['navigate'] = $day;

          // The pattern to look for.
          // Need to have " at end to ensure regular expression
          // knows where to stop when searching the pattern.
          $pattern = '/\/' . $remote_site . '\/important-dates\/calendar\/day.*?\/' . $day . '[^"]*/ms';

          // Get the replacement URL with the parameters.
          $url = url('important-dates/calendar', array('query' => $query_parameters));

          // Replace the instances.
          $calendar_html = preg_replace($pattern, $replace, $calendar_html);

          // Unset the navigate parameter, so it is not used anywhere else.
          unset($query_parameters['navigate']);
        }
      }
    }
  }

  // Set the form method to get.
  $form['#method'] = 'get';
  $form['#action'] = url('important-dates/calendar');

  // Set the view container.
  $form['view'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'view',
        'view-uw-view-important-dates-calendar',
        'uw-calendar',
      ),
    ),
  );

  // Set the header container.
  $form['view']['header'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'view-header',
      ),
    ),
  );

  // Get the tabs.
  _uw_important_dates_remote_site_get_tabs($form, $query_parameters, 'calendar', 'view');

  // Get the calendar navigation.
  _uw_important_dates_remote_site_get_nav($form, $query_parameters, $month_view);

  if ($month_view) {

    // Set the filters container.
    $form['view']['view-filters'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('view-filters')),
    );

    // Get the filters.
    _uw_important_dates_remote_site_get_filters($form, $query_parameters);

    // If there are site restrictions, add them to the parameters.
    if ($sites !== '') {
      foreach ($sites as $site) {
        $query_parameters['sites'][] = $site;
      }
    }

    _uw_important_dates_remote_site_get_calendar_message($form, $query_parameters);
  }

  // Set the content container.
  $form['view']['content'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('view-content')),
  );

  // Set the calendar HTML.
  $form['view']['content']['calendar'] = array(
    '#type' => 'markup',
    '#markup' => isset($calendar_html) ? $calendar_html : '',
  );

  return $form;
}
