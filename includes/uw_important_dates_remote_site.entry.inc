<?php

/**
 * @file
 * Functions relating to individual entries.
 */

/**
 * Helper function.
 *
 * Display the important dates entry.
 */
function _uw_important_dates_remote_site_entry($query_parameters = NULL) {

  if (!$query_parameters) {
    // Get the parameters from the URL.
    $query_parameters = uw_important_dates_get_query_parameters();
  }

  if (empty($query_parameters['id'])) {
    return drupal_not_found();
  }

  // Get the URL of the calendar entry.
  $url = 'important_dates/' . (int) $query_parameters['id'];

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request($url);

  if (isset($json_response['data'][0])) {
    $data = $json_response['data'][0];
  }
  else {
    return drupal_not_found();
  }

  // Get the current academic year from settings.
  $current_academic_year = variable_get('uw_important_dates_current_academic_year');

  // The page array.
  $page = [];

  // The container for the page.
  $page['important-dates'] = array(
    '#type' => 'container',
    '#attributes' => ['class' => ['important-dates']],
  );

  // If the academic year is less than current from settings,
  // add the ical for individual entries.
  if ($data['academic_year_tid'] >= $current_academic_year) {

    $img_variables = array(
      'path' => base_path() . drupal_get_path('module', 'uw_important_dates_remote_site') . '/css/images/add-calendar-icon-33x20.png',
      'alt' => 'Export this important date to calendar',
      // Tooltip for sighted users.
      'title' => 'Export this important date to calendar',
      'width' => '33',
      'height' => '20',
    );

    // HTML to display the calendar icon.
    $ical_html = l(
      theme('image', $img_variables),
      'important-dates/important_dates_ical_entry.ics',
      array(
        'attributes' => array(
          'class' => array('individual-event-ical'),
        ),
        'query' => array('nid' => (int) $query_parameters['id']),
        'html' => TRUE,
      )
    );

    // The HTML to be used in the title with ical.
    $title_html = '<h1>' . $data['title'] . $ical_html . '</h1>';
  }
  else {

    // The HTML to be used in the title, no ical.
    $title_html = '<h1>' . $data['title'] . '</h1>';
  }

  // The title for the page.
  $page['important-dates']['title'] = array(
    '#type' => 'markup',
    '#markup' => $title_html,
  );

  // If the academic year is an archive, add message.
  if ($data['academic_year_tid'] < $current_academic_year) {

    // Add the archive message wrapper to the form.
    $page['important-dates']['archive']['messages'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('view-id-uw_ct_special_alert_block')),
    );

    // The archive message.
    $archive_message = isset(variable_get('uw_archive_message')['value']) && variable_get('uw_archive_message')['value'] !== '' ? variable_get('uw_archive_message')['value'] : 'These important dates are provided for historical purpose.';

    // Add the archive message to the form.
    $page['important-dates']['archive']['messages']['message'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="clearfix"><p>' . $archive_message . '</p></div>',
    );
  }

  // Academic year.
  $page['important-dates']['year'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . $data['academic_year'] . '</h2>',
  );

  // Description container.
  $page['important-dates']['description'] = array(
    '#type' => 'container',
    '#attributes' => ['class' => ['important-dates__description']],
  );

  // If there is a description, display it.
  if (isset($data['description']) && !empty($data['description'])) {
    // Description.
    $page['important-dates']['description']['text'] = array(
      '#type' => 'markup',
      '#markup' => $data['description'],
    );
  }

  // Dates container.
  $page['important-dates']['dates'] = array(
    '#type' => 'container',
    '#attributes' => ['class' => ['important-dates__dates']],
  );

  // Dates wrapper container.
  $page['important-dates']['dates']['wrapper'] = array(
    '#type' => 'container',
    '#attributes' => ['class' => ['important-dates__dates--wrapper']],
  );

  $count = 0;
  foreach ($data['details'] as $detail) {
    // Counter for the wrapper, used so we get multiple instances of --wrapper.
    $wrapper = 'wrapper' . $count;

    // Dates wrapper container.
    $page['important-dates']['dates'][$wrapper] = array(
      '#type' => 'container',
      '#attributes' => ['class' => ['important-dates__dates--wrapper']],
    );

    // Academic term container.
    $page['important-dates']['dates'][$wrapper]['term'] = array(
      '#type' => 'container',
      '#attributes' => ['class' => ['important-dates__term']],
    );

    // Academic term.
    $page['important-dates']['dates'][$wrapper]['term']['item'] = array(
      '#type' => 'markup',
      '#markup' => $detail['academic_term'],
    );

    // Date container.
    $page['important-dates']['dates'][$wrapper]['date'] = array(
      '#type' => 'container',
      '#attributes' => ['class' => ['important-dates__date']],
    );

    // Get the start date.
    $start_date = date('l, F d, Y', strtotime($detail['start_date']));

    // If there is an end date, get the format and set the markup.
    if ($detail['end_date'] !== '') {

      // Get the end date.
      $end_date = date('l, F d, Y', strtotime($detail['end_date']));

      // Set the markup for the date.
      $markup = '<span class="date-display-range">';
      $markup .= '<span class="date-display-start">' . $start_date . '</span>';
      $markup .= ' to ';
      $markup .= '<span class="date-display-end">' . $end_date . '</span>';
      $markup .= '</span>';
    }
    else {
      $markup = '<span class="date-display-single">' . $start_date . '</span>';
    }

    // Date.
    $page['important-dates']['dates'][$wrapper]['date']['item'] = array(
      '#type' => 'markup',
      '#markup' => $markup,
    );

    // If there are special notes, process them.
    if (isset($detail['special_notes']) && $detail['special_notes'] !== '') {
      $page['important-dates']['dates'][$wrapper]['special_notes'] = array(
        '#type' => 'container',
        '#attributes' => ['class' => ['important-dates__special-notes']],
      );

      $page['important-dates']['dates'][$wrapper]['special_notes']['note'] = array(
        '#type' => 'markup',
        '#markup' => $detail['special_notes'],
      );
    }

    $count++;
  }

  if (isset($data['type']) && $data['type']) {
    $page['important-dates']['tags'] = array(
      '#type' => 'container',
      '#attributes' => ['class' => ['important-dates-tags']],
    );

    $page['important-dates']['tags']['tag'] = array(
      '#type' => 'container',
      '#attributes' => ['class' => ['important-dates-tag', 'important-dates-tag--type']],
    );

    $page['important-dates']['tags']['tag']['item'] = array(
      '#type' => 'markup',
      '#markup' => $data['type'],
    );
  }

  return $page;
}
