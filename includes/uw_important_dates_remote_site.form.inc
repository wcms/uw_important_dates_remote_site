<?php

/**
 * @file
 * Functions for Important Dates Remote Site forms.
 */

/**
 * Helper function.
 *
 * Menu callback for important dates config form.
 */
function _uw_important_dates_remote_site_settings_form($form, &$form_state) {

  // Load the currently logged in user.
  global $user;

  $form['uw_create_list_link'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-create-list-link')),
  );

  $form['uw_create_list_link']['link'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Create archive link'), 'admin/config/system/uw_important_dates_create_archive_link', array('attributes' => array('class' => array('uw-create-archive-link')))),
  );

  // If the user is an administrator display the server settings.
  if (in_array('administrator', $user->roles)) {

    // Fieldset for API settings.
    $form['uw_api'] = array(
      '#type' => 'fieldset',
      '#title' => t('API Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    // The API version for the central site.
    $form['uw_api']['uw_important_dates_api_version'] = array(
      '#type' => 'textfield',
      '#title' => t('Important dates API version number'),
      '#description' => t('Enter the API version number for important dates, i.e. 1.0, 1.1, 1.2, etc ...'),
      '#default_value' => variable_get('uw_important_dates_api_version'),
      '#required' => TRUE,
    );

    // The URL for the central site.
    $form['uw_api']['uw_important_dates_central_site'] = array(
      '#type' => 'textfield',
      '#title' => t('Central site URL'),
      '#description' => t('Enter the URL for the important dates central site. i.e. important-dates-central, this is the text: https://uwaterloo.ca/<central_site_url>.'),
      '#default_value' => variable_get('uw_important_dates_central_site'),
      '#required' => TRUE,
    );

    $form['uw_api']['uw_important_dates_central_site_server'] = array(
      '#type' => 'textfield',
      '#title' => t('Central site server'),
      '#description' => t('Enter the server address for the important dates central site, i.e. uwaterloo.ca, this is the text https://<central_site_server>/important-dates'),
      '#default_value' => variable_get('uw_important_dates_central_site_server'),
      '#required' => TRUE,
    );

    $form['uw_api']['uw_important_dates_remote_site'] = array(
      '#type' => 'textfield',
      '#title' => t('Remote site URL'),
      '#description' => t('Enter the URL for the remote site url, i.e. important-dates-remote, this is the text after https://uwaterloo.ca/<remote_site_url>.'),
      '#default_value' => variable_get('uw_important_dates_remote_site'),
      '#required' => TRUE,
    );

    $form['uw_api']['uw_important_dates_remote_site_server'] = array(
      '#type' => 'textfield',
      '#title' => t('Remote site server'),
      '#description' => t('Enter the server address for the important dates remote site, i.e. uwaterloo.ca, this is the text https://<remote_site_server>/important-dates'),
      '#default_value' => variable_get('uw_important_dates_remote_site_server'),
      '#required' => TRUE,
    );

    // Select for the content to be displayed.
    $form['uw_api']['uw_important_dates_ignore_certificates'] = array(
      '#type' => 'checkbox',
      '#title' => t('Ignore invalid certificates'),
      '#default_value' => variable_get('uw_important_dates_ignore_certificates', 0),
      '#description' => t('Ignore invalid certificates when connecting to central site'),
      '#required' => FALSE,
    );
  }

  // Variable to handle error messages for query failures.
  $query_failure = FALSE;

  // Only get these settings if there is a central site URL.
  if (variable_get('uw_important_dates_central_site', NULL) && variable_get('uw_important_dates_central_site_server', NULL)) {

    // Get the academic years from the central site.
    $academic_years_options = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_academic_year');

    // Sort the academic years by alpha on the value.
    asort($academic_years_options);

    if ($academic_years_options) {

      // Fieldset for academic year settings.
      $form['uw_academic_year'] = array(
        '#type' => 'fieldset',
        '#title' => t('Academic Year Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );

      // Select drop down for current academic year.
      // This will be the academic year displayed on the important dates site.
      $form['uw_academic_year']['uw_important_dates_current_academic_year'] = array(
        '#type' => 'select',
        '#title' => 'Current academic year',
        '#options' => $academic_years_options,
        '#empty_option' => '- Select a value -',
        '#required' => TRUE,
        '#default_value' => variable_get('uw_important_dates_current_academic_year'),
      );

      // Select drop down for academic years to used in filters.
      $form['uw_academic_year']['uw_important_dates_academic_years'] = array(
        '#type' => 'select',
        '#title' => t('Academic years to be used in filters'),
        '#multiple' => TRUE,
        '#options' => $academic_years_options,
        '#description' => t('Select the academic year(s) that are to be used in the filters.'),
        '#default_value' => variable_get('uw_important_dates_academic_years'),
        '#required' => TRUE,
        '#empty_option' => '- Select a value -',
      );

    }
    elseif (empty($form_state['input'])) {
      $query_failure = TRUE;
    }

    // Get the academic years from the central site.
    $audience_options = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_audiences');

    // Get the academic years from the central site.
    $sites_options = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_sites');

    if ($audience_options || $sites_options) {

      // Fieldset for content settings.
      $form['uw_content'] = array(
        '#type' => 'fieldset',
        '#title' => t('Content Settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
      );
    }

    // If there are audience options, add to form.
    if ($audience_options) {

      // Select for the audiences to be used in the audience filter.
      $form['uw_content']['uw_important_dates_audiences'] = array(
        '#type' => 'select',
        '#title' => t('Audiences to be used in the audience filter'),
        '#multiple' => TRUE,
        '#options' => $audience_options,
        '#default_value' => variable_get('uw_important_dates_audiences', ''),
        '#description' => t('Select audiences to be used in the audience filter.'),
        '#required' => TRUE,
        '#empty_option' => '- Select a value -',
      );
    }
    elseif (empty($form_state['input'])) {
      $query_failure = TRUE;
    }

    if ($sites_options) {
      // Select for the content to be displayed.
      $form['uw_content']['uw_important_dates_sites'] = array(
        '#type' => 'select',
        '#title' => t('Content to get from central site'),
        '#multiple' => TRUE,
        '#options' => $sites_options,
        '#default_value' => variable_get('uw_important_dates_sites', ''),
        '#description' => t('Select the content to get from the central site.'),
        '#required' => TRUE,
        '#empty_option' => '- Select a value -',
      );
    }
    elseif (empty($form_state['input'])) {
      $query_failure = TRUE;
    }
  }

  // If there is a failure in a query, display an error message.
  if ($query_failure) {
    drupal_set_message('The central site is not setup correctly, please ensure that settings are correct and try again.', 'error', FALSE);
  }

  // Set the start year based on the variable or current year.
  $start_year = variable_get('uw_start_year_calendar', date('Y'));

  // Get the start year options.
  $start_year_options = _uw_important_dates_remote_site_get_year_ranges(($start_year - 4), ($start_year + 4));

  // Fieldset for calendar settings.
  $form['uw_calendar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calendar Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  // Select drop down for the start year calendar view.
  $form['uw_calendar']['uw_start_year_calendar'] = array(
    '#type' => 'select',
    '#title' => t('Start year for calendar view'),
    '#multiple' => FALSE,
    '#options' => $start_year_options,
    '#description' => t('Select the start year when viewing the calendar.'),
    '#default_value' => variable_get('uw_start_year_calendar'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Set the end year based on the variable or current year.
  $end_year = variable_get('uw_end_year_calendar', date('Y'));

  // Get the end year options.
  $end_year_options = _uw_important_dates_remote_site_get_year_ranges(($end_year - 6), ($end_year + 6));

  // Select drop down for the start year calendar view.
  $form['uw_calendar']['uw_end_year_calendar'] = array(
    '#type' => 'select',
    '#title' => t('End year for calendar view'),
    '#multiple' => FALSE,
    '#options' => $end_year_options,
    '#description' => t('Select the end year when viewing the calendar.'),
    '#default_value' => variable_get('uw_end_year_calendar'),
    '#required' => TRUE,
    '#empty_option' => '- Select a value -',
  );

  // Fieldset for archive settings.
  $form['uw_archives'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['uw_archives']['uw_archive_message_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Message'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#description' => 'Enter a message to be displayed when on an archive page.  If left blank, the message will display as: "These important dates are provided for historical purpose.".',
  );

  // Message for archive pages.
  $form['uw_archives']['uw_archive_message_wrapper']['uw_archive_message'] = array(
    '#type' => 'text_format',
    '#default_value' => variable_get('uw_archive_message') ? variable_get('uw_archive_message')['value'] : '',
    '#format' => 'uw_tf_basic',
  );

  // Set variable uw_important_dates_archive_exclude: Dates having these
  // taxonomy terms are to be excluded from archives.
  $important_dates_types = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_types');
  $form['uw_archives']['uw_important_dates_archive_exclude'] = [
    '#type' => 'checkboxes',
    '#title' => t('Date types to exclude from archive'),
    '#options' => $important_dates_types,
    '#default_value' => variable_get('uw_important_dates_archive_exclude', []),
  ];

  // Fieldset for archive link settings.
  $form['uw_archives']['uw_archive_link_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Archive Link Settings'),
    '#collapsible' => FALSE,
  );

  // Archive link text.
  $form['uw_archives']['uw_archive_link_settings']['uw_archive_link_text'] = array(
    '#type' => 'textfield',
    '#title' => 'Archive link text',
    '#default_value' => variable_get('uw_archive_link_text'),
    '#description' => 'Enter the text that will be in the link to the archive list page.',
  );

  // Actual archive link, must be fully qualified URL.
  $form['uw_archives']['uw_archive_link_settings']['uw_archive_link'] = array(
    '#type' => 'textfield',
    '#title' => 'Archive link',
    '#default_value' => variable_get('uw_archive_link'),
    '#description' => t('Enter a full URL to the archive page, i.e. https://uwaterloo.ca/important-dates/archives-list.'),
  );

  // Add validation to the form.
  $form['#validate'][] = '_uw_important_dates_remote_site_settings_validate';

  return system_settings_form($form);
}

/**
 * Helper function.
 *
 * Getting the navigation for the Important Dates views.
 */
function _uw_important_dates_remote_site_get_nav(&$form, $query_parameters, $month_view = TRUE) {

  // If there is a navigate parameter, set year and month accordingly.
  // If there is no navigate parameter, set year and month to current.
  if (isset($query_parameters['navigate'])) {
    $date = explode('-', $query_parameters['navigate']);
    $year = $date[0];
    $month = $date[1];

    if (!$month_view) {
      $day = $date[2];

      // If the day is less than 10 add the leading zero.
      if ($day < 10 && $day[0] !== '0') {
        $day = '0' . $day;
      }
    }
  }
  else {
    $year = date('Y');
    $month = date('m');
  }

  // Get start year for navigation.
  $start_year = variable_get('uw_start_year_calendar', date('Y') - 4);

  // Get start year for navigation.
  $end_year = variable_get('uw_end_year_calendar', date('Y') + 4);

  // Get the range of year options.
  $year_options = _uw_important_dates_remote_site_get_year_ranges($start_year, $end_year);

  // Get all the months.
  for ($i = 1; $i <= 12; $i++) {
    if ($i < 10) {
      $month_options['0' . $i] = uw_month_name_short($i);
    }
    else {
      $month_options[$i] = uw_month_name_short($i);
    }
  }

  // Nav wrapper.
  $form['view']['header']['nav-wrapper'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('date-nav-wrapper', 'clearfix')),
  );

  // Nav.
  $form['view']['header']['nav-wrapper']['nav'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('date-nav')),
  );

  /***** Previous *****/
  $form['view']['header']['nav-wrapper']['nav']['prev_container'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('date-prev')),
  );

  // Add a parameter so that we know we are on the calendar navigation.
  $query_parameters['op'] = 'nav';

  // Get the previous link parameters.
  $prev_parameters = _uw_important_dates_remote_site_get_next_or_prev_parameters($query_parameters, 'prev', $month_view);

  $options = array(
    'query' => $prev_parameters,
    'attributes' => array(
      'class' => array('uw-imp-dates-loader'),
    ),
  );

  $form['view']['header']['nav-wrapper']['nav']['prev_container']['prev'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Previous'), '/important-dates/calendar', $options),
  );
  /***** End Previous *****/

  /***** Heading *****/
  $form['view']['header']['nav-wrapper']['nav']['heading'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('date-heading')),
  );

  // Get the month name based on the month given.
  $month_name = date('F', mktime(0, 0, 0, $month, 10));

  // If month view, set header title month_name and year.
  // If not month view, set header title month_name day, year.
  if ($month_view) {
    $header_markup = $month_name . ' ' . $year;
  }
  else {
    if ($day < 10) {
      $day = $day[1];
    }

    $header_markup = $month_name . ' ' . $day . ', ' . $year;
  }

  $form['view']['header']['nav-wrapper']['nav']['heading']['header'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . $header_markup . '</h2>',
    '#attributes' => array('class' => array('date-heading')),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('navigate-calendar')),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar'] = array(
    '#type' => 'container',
    '#attributes' => array('id' => 'edit-navigate-calendar'),
  );
  /***** End Heading *****/

  /**** Year ****/
  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['year'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('navigate-calendar__year')),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['year']['form-item'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-item',
        'form-type-select',
        'form-item-year',
      ),
    ),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['year']['form-item']['select'] = array(
    '#default_value' => isset($year) ? $year : date('Y'),
    '#type' => 'select',
    '#options' => $year_options,
    '#name' => 'year',
    '#title' => 'Year',
    '#id' => 'important-dates-year',
  );
  /**** End Year ****/

  /**** Month ****/
  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['month'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('navigate-calendar__month')),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['month']['form-item'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'form-item',
        'form-type-select',
        'form-item-year',
      ),
    ),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['month']['form-item']['select'] = array(
    '#type' => 'select',
    '#options' => $month_options,
    '#default_value' => isset($month) ? $month : date('m'),
    '#name' => 'month',
    '#title' => 'Month',
    '#id' => 'important-dates-month',
  );
  /**** End Month ****/

  /**** Submit *****/
  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['submit'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('navigate-calendar__submit')),
  );

  $form['view']['header']['nav-wrapper']['nav']['heading']['navigate']['calendar']['submit']['button'] = array(
    '#type' => 'submit',
    '#value' => 'Apply',
    '#attributes' => array(
      'class' => array('small-button', 'form-submit', 'uw-imp-dates-loader'),
    ),
  );
  /***** End Submit ******/

  /***** Next *****/
  $form['view']['header']['nav-wrapper']['nav']['next_container'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('date-next')),
  );

  // Get the next link parameters.
  $next_parameters = _uw_important_dates_remote_site_get_next_or_prev_parameters($query_parameters, 'next', $month_view);

  $options = array(
    'query' => $next_parameters,
    'attributes' => array(
      'class' => array('uw-imp-dates-loader'),
    ),
  );

  $form['view']['header']['nav-wrapper']['nav']['next_container']['prev'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Next'), '/important-dates/calendar', $options),
  );
  /***** End Next *****/
}

/**
 * Helper function.
 *
 * Get the filters for the list view.
 */
function _uw_important_dates_remote_site_get_filters(&$form, $query_parameters) {
  $form['view']['view-filters']['exposed'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-form')),
  );

  $form['view']['view-filters']['exposed']['widgets'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widgets', 'clearfix')),
  );

  $form['view']['view-filters']['exposed']['widgets']['type'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widget', 'views-widget-filter-types')),
  );

  $type_options = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_types');
  asort($type_options);

  $form['view']['view-filters']['exposed']['widgets']['type']['select'] = array(
    '#type' => 'select',
    '#title' => 'Type',
    '#name' => 'type',
    '#id' => 'edit-type',
    '#options' => $type_options,
    '#empty_option' => '- Any -',
    '#id' => 'important-dates-type',
    '#default_value' => isset($query_parameters['type']) ? $query_parameters['type'] : '',
  );

  $form['view']['view-filters']['exposed']['widgets']['audience'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widget', 'views-widget-filter-audience')),
  );

  // Audiences taxonomy from central site.
  $audiences = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_audiences');

  // Audiences to be used in filter from settings.
  $audiences_setting = variable_get('uw_important_dates_audiences');

  // Step through each audience and check if we are
  // to use it as an option in the audiences filter.
  foreach ($audiences as $key => $audience) {
    if (in_array($key, $audiences_setting)) {
      $audience_options[$key] = $audience;
    }
  }

  $form['view']['view-filters']['exposed']['widgets']['audience']['select'] = array(
    '#type' => 'select',
    '#title' => 'Audience',
    '#name' => 'audience',
    '#id' => 'important-dates-audience',
    '#options' => $audience_options,
    '#empty_option' => '- Any -',
    '#default_value' => isset($query_parameters['audience']) ? $query_parameters['audience'] : '',
  );

  $form['view']['view-filters']['exposed']['widgets']['search'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widget', 'views-widget-filter-combine')),
  );

  $form['view']['view-filters']['exposed']['widgets']['search']['input'] = array(
    '#type' => 'textfield',
    '#title' => 'Keyword(s)',
    '#id' => 'edit-combine',
    '#name' => 'combine',
    '#default_value' => isset($query_parameters['combine']) ? $query_parameters['combine'] : '',
  );

  $form['view']['view-filters']['exposed']['widgets']['submit'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widget', 'views-submit-button')),
  );

  $form['view']['view-filters']['exposed']['widgets']['submit']['input'] = array(
    '#type' => 'submit',
    '#name' => 'op',
    '#value' => 'Search',
    '#attributes' => array('class' => array('uw-imp-dates-loader')),
  );

  $form['view']['view-filters']['exposed']['widgets']['reset'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('views-exposed-widget', 'views-reset-button')),
  );

  $form['view']['view-filters']['exposed']['widgets']['reset']['input'] = array(
    '#type' => 'submit',
    '#name' => 'op',
    '#value' => 'Reset',
    '#attributes' => array('class' => array('uw-imp-dates-loader')),
  );
}

/**
 * Helper function.
 *
 * Get the filters for the list view.
 */
function _uw_important_dates_remote_site_get_list_filters(&$form, $query_parameters) {
  // The filters container.
  $form['uw-important-dates']['filters'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('important-dates-filters')),
  );

  // Get the academic year and add the Any option.
  $academic_years = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_academic_year');

  // Alpha sort the academic years.
  asort($academic_years);

  $academic_years = array('Any' => '- Any -') + $academic_years;

  $academic_years_setting = variable_get('uw_important_dates_academic_years', []);

  // ISTWCMS-3024.
  // Step through each academic year in the filter.
  // Check if it is in the settings and is an integer.
  // If so, keep it in the filters, if not remove it.
  foreach ($academic_years as $key => $value) {
    if (in_array($key, $academic_years_setting) && is_int($key)) {
      $years = explode(' - ', $value);
      $academic_years[$key] = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1];
    }
    else {
      unset($academic_years[$key]);
    }
  }

  // The academic year filter drop down.
  $form['uw-important-dates']['filters']['academic_year'] = array(
    '#type' => 'select',
    '#title' => 'Academic year',
    '#options' => $academic_years,
    '#default_value' => isset($query_parameters['academic_year']) ? $query_parameters['academic_year'] : variable_get('uw_important_dates_current_academic_year'),
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-year')),
  );

  // Get the academic terms, that are not sorted, but contain weight.
  $academic_terms_raw = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_academic_terms');

  // Sort the terms.
  ksort($academic_terms_raw);

  // Step through and get the academic term array, used as options.
  $academic_terms = [];
  foreach ($academic_terms_raw as $academic_term_raw) {
    $academic_terms[$academic_term_raw['id']] = $academic_term_raw['term'];
  }

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['academic_term'] = array(
    '#type' => 'select',
    '#title' => 'Academic term',
    '#options' => $academic_terms,
    '#empty_option' => '- Any -',
    '#default_value' => isset($query_parameters['academic_term']) ? $query_parameters['academic_term'] : '',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__academic-term')),
  );

  // If there is a academic year parameter, check date drop down.
  // If there is no academic year parameter, we are in the current
  // academic year and set default to parameter and not disabled.
  if (isset($query_parameters['academic_year'])) {

    // Get the current academic year from settings.
    $current_academic_year = variable_get('uw_important_dates_current_academic_year');

    // If the academic year and current year are not the same,
    // default to all and set drop down to disabled.
    // If they are the same, set default to parameter or today,
    // and do not have the drop down disabled.
    if ((int) $query_parameters['academic_year'] !== (int) $current_academic_year) {
      $date_option = 'All';
      $date_disabled = TRUE;
    }
    else {
      $date_option = isset($query_parameters['date']) ? $query_parameters['date'] : 'Today';
      $date_disabled = FALSE;
    }
  }
  else {
    $date_option = isset($query_parameters['date']) ? $query_parameters['date'] : 'Today';
    $date_disabled = FALSE;
  }

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['date'] = array(
    '#type' => 'select',
    '#title' => 'Dates',
    '#options' => array(
      'Any' => 'All',
      'Today' => 'Current and upcoming',
    ),
    '#default_value' => $date_option,
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__date')),
    '#disabled' => $date_disabled,
  );

  // The options for the type of important date.
  $type_options = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_types');
  asort($type_options);

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['type'] = array(
    '#type' => 'select',
    '#title' => 'Type',
    '#options' => $type_options,
    '#default_value' => isset($query_parameters['type']) ? $query_parameters['type'] : '',
    '#empty_option' => '- Any -',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__type')),
  );

  // Audiences taxonomy from central site.
  $audiences = _uw_important_dates_remote_site_get_taxonomy_from_central_site('important_dates_audiences');

  // Audiences to be used in filter from settings.
  $audiences_setting = variable_get('uw_important_dates_audiences');

  // Step through each audience and check if we are
  // to use it as an option in the audiences filter.
  $audience_options = [];
  foreach ($audiences as $key => $audience) {
    if (in_array($key, $audiences_setting)) {
      $audience_options[$key] = $audience;
    }
  }

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['audience'] = array(
    '#type' => 'select',
    '#title' => 'Audience',
    '#options' => $audience_options,
    '#default_value' => isset($query_parameters['audience']) ? $query_parameters['audience'] : '',
    '#empty_option' => '- Any -',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__audience')),
  );

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['search'] = array(
    '#type' => 'textfield',
    '#title' => 'Keyword(s)',
    '#prefix' => '<div class="important-dates-filters-filter">',
    '#suffix' => '</div>',
    '#attributes' => array('class' => array('important-dates-filters__search-button'), 'size' => 30),
    '#default_value' => isset($query_parameters['combine']) ? $query_parameters['combine'] : '',
  );

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['apply'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#attributes' => array('class' => array('small-button')),
    '#prefix' => '<div class="important-dates-filters-filter important-dates-filters__button">',
    '#suffix' => '</div>',
    '#submit' => array('_uw_important_dates_remote_site_filters_submit'),
    '#attributes' => array('class' => array('important-dates-filters__apply', 'uw-imp-dates-loader')),
  );

  // The academic term filter drop down.
  $form['uw-important-dates']['filters']['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
    '#attributes' => array('class' => array('small-button')),
    '#prefix' => '<div class="important-dates-filters-filter important-dates-filters__button">',
    '#suffix' => '</div>',
    '#submit' => array('_uw_important_dates_remote_site_filters_submit'),
    '#attributes' => array('class' => array('important-dates-filters__reset', 'uw-imp-dates-loader')),
  );
}
