<?php

/**
 * @file
 * Functions for Important Dates Remote Site.
 */

/**
 * Helper function.
 *
 * Return the array with the list of date ranges.
 */
function _uw_important_dates_remote_site_get_year_ranges($low, $high) {

  // Step through the range and get the dates in an array.
  foreach (range($low, $high) as $key) {
    $options[$key] = t('@range_dates', array('@range_dates' => $key));
  }

  return $options;
}

/**
 * Helper function.
 *
 * Getting the tabs for the Important Dates views.
 */
function _uw_important_dates_remote_site_get_tabs(&$form, $query_parameters, $active, $first_element) {
  global $_uw_important_dates_remote_site_num_of_entries;

  $options = array(
    'query' => $query_parameters,
    'attributes' => array(
      'class' => array('uw-imp-dates-loader'),
    ),
  );

  $items = [];
  $items[] = l(t('List View'), 'important-dates/list', $options);
  if ($_uw_important_dates_remote_site_num_of_entries > 0) {
    $items[] = l(t('Export Important Dates'), 'important-dates/important_dates_ical.ics', ['query' => $query_parameters]);
  }
  $items[] = l(t('Calendar View'), 'important-dates/calendar', $options);
  $form[$first_element]['header']['tabs'] = [
    '#theme' => 'item_list',
    '#type' => 'ul',
    '#attributes' => ['class' => ['important-dates-tabs']],
    '#items' => $items,
    '#prefix' => '<nav aria-label="Date views">',
    '#suffix' => '</nav>',
  ];
}

/**
 * Helper function.
 *
 * Get the messages for the calendar view.
 */
function _uw_important_dates_remote_site_get_calendar_message(&$form, $query_parameters) {

  global $_uw_important_dates_remote_site_num_of_entries;

  // If there are no entries in the set the no results message.
  if ($_uw_important_dates_remote_site_num_of_entries == 0) {

    // Container for the message.
    $form['view']['messages'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('important-dates-calendar-message')),
    );

    // The actual message.
    $form['view']['messages']['message'] = array(
      '#type' => 'markup',
      '#markup' => 'No results found',
    );
  }
}

/**
 * Helper function.
 *
 * Return the parameters for the previous or next links.
 */
function _uw_important_dates_remote_site_get_next_or_prev_parameters($query_parameters, $type, $month_view = TRUE) {

  // If we have a blank type of not set to prev or next, return nothing.
  if (!in_array($type, ['prev', 'next'])) {
    return;
  }

  // If there is a navigate parameters set year and month based on it.
  // If there is no navigate parameter, set to today's year and month.
  if (!isset($query_parameters['navigate'])) {
    if ($month_view) {
      $query_parameters['navigate'] = date('Y-m');
    }
    else {
      $query_parameters['navigate'] = date('Y-m-d');
    }
  }

  // If on a month view, set navigate based on month.
  // If not on a month view, set navigate based day.
  if ($month_view) {
    if ($type == 'prev') {
      $query_parameters['navigate'] = date('Y-m', strtotime($query_parameters['navigate'] . " -1 month"));
    }
    else {
      $query_parameters['navigate'] = date('Y-m', strtotime($query_parameters['navigate'] . " +1 month"));
    }
  }
  else {
    if ($type == 'prev') {
      $query_parameters['navigate'] = date('Y-m-d', strtotime($query_parameters['navigate'] . " -1 day"));
    }
    else {
      $query_parameters['navigate'] = date('Y-m-d', strtotime($query_parameters['navigate'] . " +1 day"));
    }
  }

  // Return the new parameters.
  return $query_parameters;
}

/**
 * Helper function.
 *
 * Get the taxonomy term name from central site using tid.
 */
function _uw_important_dates_remote_site_get_taxonomy_name($tid, $taxonomy_name, $academic_year_split = TRUE) {

  // Set the taxonomy.
  $taxonomy = '';

  // Get the taxonomy list from the central site.
  $taxonomy_list = _uw_important_dates_remote_site_get_taxonomy_from_central_site($taxonomy_name);

  // If academic terms taxonomy, step through and get term.
  // If not academic terms, see if ket is in array and set
  // the taxonomy.
  if ($taxonomy_name == 'important_dates_academic_terms') {

    // Step through each taxonomy in the list and check
    // if the tid is in the array.
    foreach ($taxonomy_list as $tax) {

      // If tid is in array, set the taxonomny.
      if ($tax['id'] == $tid) {
        $taxonomy = $tax['term'];
        break;
      }
    }
  }
  else {

    // If the key is in the list, set the taxonomy.
    if (array_key_exists($tid, $taxonomy_list)) {
      $taxonomy = $taxonomy_list[$tid];
    }
  }

  // If the taxonomy is the academic year, set the display.
  if ($taxonomy_name == 'important_dates_academic_year' && $academic_year_split) {
    $years = explode(' - ', $taxonomy);
    $taxonomy = 'Sep. 1, ' . $years[0] . ' to Aug. 31, ' . $years[1];
  }

  return $taxonomy;
}

/**
 * Helper function.
 *
 * Set the parameters after the apply or reset for filters is clicked.
 */
function _uw_important_dates_remote_site_filters_submit($form, &$form_state) {

  // If the apply button is clicked set the parameters.
  // If the reset button is clicked remove all parameters.
  if ($form_state['clicked_button']['#value'] == 'Search') {

    // If there is an academic year add as a parameter.
    if (isset($form_state['values']['academic_year'])) {
      $parameters['academic_year'] = $form_state['values']['academic_year'];
    }

    // If there is an academic term add as a parameter.
    if (isset($form_state['values']['academic_term']) && $form_state['values']['academic_term'] !== '') {
      $parameters['academic_term'] = $form_state['values']['academic_term'];
    }

    // If there is an important type add as a parameter.
    if (isset($form_state['values']['type']) && $form_state['values']['type'] !== '') {
      $parameters['type'] = $form_state['values']['type'];
    }

    // If there is an audience add as a parameter.
    if (isset($form_state['values']['audience']) && $form_state['values']['audience'] !== '') {
      $parameters['audience'] = $form_state['values']['audience'];
    }

    // If there is a search add as a parameter.
    if (isset($form_state['values']['search']) && $form_state['values']['search'] !== '') {
      $parameters['combine'] = $form_state['values']['search'];
    }

    if (isset($form_state['values']['date'])) {
      $parameters['date'] = $form_state['values']['date'];
    }
    else {
      $parameters['date'] = 'Today';
    }
  }
  else {
    $parameters = array();
  }

  // Go to the page with the parameters.
  drupal_goto('important-dates/list', array('query' => $parameters));
}

/**
 * Helper function.
 *
 * Get the archive dates and message for list view.
 */
function _uw_important_dates_remote_site_get_archive_list(&$form, $query_parameters) {

  // Get the academic year taxonomy name.
  $academic_year_taxonomy = _uw_important_dates_remote_site_get_taxonomy_name($query_parameters['academic_year'], 'important_dates_academic_year');

  // If the academic term is set, get taxonomy name.
  if (isset($query_parameters['academic_term'])) {
    $academic_term_taxonomy = _uw_important_dates_remote_site_get_taxonomy_name($query_parameters['academic_term'], 'important_dates_academic_terms');
  }

  // Form wrapper for archive.
  $form['uw-important-dates']['archive'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-imp-dates-archives')),
  );

  // Add the archive message wrapper to the form.
  $form['uw-important-dates']['archive']['messages'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('view-id-uw_ct_special_alert_block')),
  );

  // The archive message.
  $archive_message = isset(variable_get('uw_archive_message')['value']) && variable_get('uw_archive_message')['value'] !== '' ? variable_get('uw_archive_message')['value'] : 'These important dates are provided for historical purpose.';

  // Add the archive message to the form.
  $form['uw-important-dates']['archive']['messages']['message'] = array(
    '#type' => 'markup',
    '#markup' => '<div class="clearfix"><p>' . $archive_message . '</p></div>',
  );

  // Add the date message wrapper to the form.
  $form['uw-important-dates']['archive']['date'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-imp-dates-archives-date')),
  );

  // Set the date message.
  $message = $academic_year_taxonomy;

  // If there is an academic term, add to the date message.
  if (isset($academic_term_taxonomy)) {
    $message .= ' - ' . $academic_term_taxonomy;
  }

  // Add the date message to the form.
  $form['uw-important-dates']['archive']['date']['message'] = array(
    '#type' => 'markup',
    '#markup' => '<h2>' . $message . '</h2>',
  );
}

/**
 * Helper function.
 *
 * Form validation for important date settings.
 */
function _uw_important_dates_remote_site_settings_validate($form, &$form_state) {

  // If there are values set for archive link text and the actual link,
  // process the archive link text and link.
  if (isset($form_state['values']['uw_archive_link_text']) || isset($form_state['values']['uw_archive_link'])) {

    // If either are not blank, continue to process.
    if ($form_state['values']['uw_archive_link_text'] !== '' || $form_state['values']['uw_archive_link'] !== '') {

      // If the link text is blank, show error on link text.
      if ($form_state['values']['uw_archive_link_text'] == '') {
        form_set_error('uw_archive_link_text', 'Archive link text must not be blank.');
      }
      // If the link is blank, show error on the link.
      elseif ($form_state['values']['uw_archive_link'] == '') {
        form_set_error('uw_archive_link', 'Archive link must not be blank.');
      }
      // Both link text and link have something in them,
      // continue to process and validate link.
      else {

        // Check if URL is valid, if not show error on link.
        if (!valid_url($form_state['values']['uw_archive_link'], TRUE)) {
          form_set_error('uw_archive_link', 'Archive link must valid and absolute.');
        }
      }
    }
  }

  // If the currrent academic year is not in the academic years
  // to be used in filters, show error and do not save form.
  if (!in_array($form_state['values']['uw_important_dates_current_academic_year'], $form_state['values']['uw_important_dates_academic_years'])) {

    // Set the error.
    form_set_error('uw_important_dates_academic_years', 'The current academic year must be included in the academic years to be used in filters.');
  }
}
