<?php

/**
 * @file
 * Functions for ical for Important Dates Remote Site.
 */

/**
 * Helper function.
 *
 * Displays the ical.
 */
function _uw_important_dates_remote_site_ical() {

  // Get the parameters from the URL.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request('important_dates_remote_ical', $query_parameters);

  // Get the central site URL, this will be search pattern.
  $central_site = variable_get('uw_important_dates_central_site_server') . '/' . variable_get('uw_important_dates_central_site');

  // Get the remote site URL, this will be the replacement pattern.
  $remote_site = variable_get('uw_important_dates_remote_site_server') . '/' . variable_get('uw_important_dates_remote_site');

  // Replace the central site with the remote site on ical.
  $ical = str_replace($central_site, $remote_site, $json_response['data'][0]['ical']);

  // Add the http header to use ical.
  drupal_add_http_header('Content-Type', 'text/calendar');

  // Output the actual ical.
  print $ical;
}

/**
 * Helper function.
 *
 * Displays the ical individual entry.
 */
function _uw_important_dates_remote_site_ical_individual() {

  // Get the parameters from the URL.
  $query_parameters = uw_important_dates_get_query_parameters();

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request('important_dates_remote_ical_individual', $query_parameters);

  // Get the central site URL, this will be search pattern.
  $central_site = variable_get('uw_important_dates_central_site_server') . '/' . variable_get('uw_important_dates_central_site');

  // Get the remote site URL, this will be the replacement pattern.
  $remote_site = variable_get('uw_important_dates_remote_site_server') . '/' . variable_get('uw_important_dates_remote_site');

  // Replace the central site with the remote site on ical.
  $ical = str_replace($central_site, $remote_site, $json_response['data'][0]['ical']);

  // Add the http header to use ical.
  drupal_add_http_header('Content-Type', 'text/calendar');

  // Output the actual ical.
  print $ical;
}
