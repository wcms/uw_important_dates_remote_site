<?php

/**
 * @file
 * Functions for Important Dates Remote Site list.
 */

/**
 * Helper function.
 *
 * Display the important dates list view.
 */
function _uw_important_dates_remote_site_list($form, &$form_state, $query_parameters = NULL) {

  global $_uw_important_dates_remote_site_num_of_entries;

  $args = arg();

  if (!$query_parameters) {
    // Get the parameters from the URL.
    $query_parameters = uw_important_dates_get_query_parameters();
  }

  $sites = variable_get('uw_important_dates_sites');

  // If the sites variable is set, add it to the parameters.
  if ($sites) {
    $query_parameters['sites'] = $sites;
  }

  // Tracking if we need to reset the academic year in the request.
  $reset_academic_year = FALSE;

  // If there is an academic year, process it and ensure that we are
  // using the correct academic year.
  // If there is academic year, set it the default that is a variable.
  if (isset($query_parameters['academic_year'])) {

    // If the academic year is blank or NULL set the default that is a variable.
    if (!$query_parameters['academic_year']) {
      $query_parameters['academic_year'] = variable_get('uw_important_dates_current_academic_year');
    }
    // If it is set to Any, set the reset tracking.
    elseif ($query_parameters['academic_year'] == 'Any') {
      $reset_academic_year = TRUE;
    }
  }
  else {
    $query_parameters['academic_year'] = variable_get('uw_important_dates_current_academic_year');
  }

  // If there is a reset for academic year, set variable to
  // remember academic year and unset the parameter.
  // We are only doing this because the parameters can not
  // have a value of Any, it is either a number or blank.
  if ($reset_academic_year) {
    $academic_year = $query_parameters['academic_year'];
    unset($query_parameters['academic_year']);
  }

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request('important-dates/list-remote', $query_parameters);
  // Default to empty array if no value returned.
  $json_response = $json_response ? $json_response : [];

  // If there is a reset for academic year, add back to parameters.
  if ($reset_academic_year) {
    $query_parameters['academic_year'] = $academic_year;
  }

  // The parameters that are going to be used for the list view count.
  $count_parameters = $query_parameters;

  // If there is no academic year parameter, set it.
  if (!isset($count_parameters['academic_year'])) {
    $count_parameters['academic_year'] = variable_get('uw_important_dates_current_academic_year');
  }

  // If there is no date parameter, set it.
  if (!isset($count_parameters['date'])) {
    $count_parameters['date'] = 'Today';
  }

  // If there is a page in the parameters, unset it.
  // This is done so that we get the entire row count.
  unset($count_parameters['page']);

  // Unset the sites to be collected, so that it is not included in links.
  unset($query_parameters['sites']);

  // If there is an order parameter, set variable to use and unset parameter.
  // If there is no order parameter, default to start_time.
  $order_parameter = isset($query_parameters['sort_by']) ? $query_parameters['sort_by'] : 'field_uw_imp_dates_date';
  unset($query_parameters['sort_by']);

  // If there is an sort parameter, set variable to use and unset parameter.
  // If there is no sort parameter, default to desc.
  $sort_parameter = isset($query_parameters['sort_order']) ? $query_parameters['sort_order'] : 'DESC';
  unset($query_parameters['sort_order']);

  // Array to store information about table sorting headers.
  $orders['terms_and_deadlines'] = array(
    'name' => 'Title',
    'sort_by' => 'title',
    'sort_order' => 'DESC',
  );

  $orders['description'] = array(
    'data' => 'Description',
  );

  $orders['academic_term'] = array(
    'name' => 'Academic term',
    'sort_by' => 'name',
    'sort_order' => 'DESC',
  );

  $orders['date'] = array(
    'name' => 'Date',
    'sort_by' => 'field_uw_imp_dates_date_value_1',
    'sort_order' => 'DESC',
  );

  // Step through each of the table orders and process them.
  foreach ($orders as $key => $value) {

    // If we are not on the description column (it is not sortable).
    if ($key !== 'description') {

      // Set the default variables for each order.
      $orders[$key]['parameters'] = $query_parameters;
      $orders[$key]['parameters']['sort_by'] = $orders[$key]['sort_by'];
      $orders[$key]['parameters']['sort_order'] = 'DESC';
      $orders[$key]['th_class'] = array('sortable', $orders[$key]['sort_by']);
      $orders[$key]['link_class'] = 'sortable-link';

      // If the order is the sort parameter, process it.
      if ($orders[$key]['sort_by'] == $order_parameter) {

        // If it is set to sort asc, set query parameter to desc and set th and
        // link classes. If it nor sort asc, set query parameter to asc and set
        // th and link classes.
        if ($sort_parameter == 'ASC') {
          $orders[$key]['parameters']['sort_order'] = 'DESC';
          $orders[$key]['th_class'] = array(
            'sortable',
            'active',
            $orders[$key]['sort_by'],
          );
          $orders[$key]['link_class'] = array(
            'sortable-link',
            'asc',
          );
        }
        else {
          $orders[$key]['parameters']['sort_order'] = 'ASC';
          $orders[$key]['th_class'] = array(
            'sortable',
            'active',
            $orders[$key]['sort_by'],
          );
          $orders[$key]['link_class'] = array(
            'sortable-link',
            'desc',
          );
        }
      }
    }
    else {
      $orders[$key]['th_class'] = array($orders[$key]['data']);
    }
  }

  // Set empty arrays for headers and rows.
  $headers = array();
  $rows = array();

  // Set through each javson request and get the response data.
  foreach ($json_response as $response_data) {

    // Get the academic year.
    $academic_year = $response_data['Academic year'];

    // Divide into years from the academic year.
    $years = explode(' - ', $academic_year);

    // If the academic term is fall add on first year
    // from the academic year.
    // If not Fall add on second year from academic year.
    if ($response_data['Academic term'] == 'Fall') {
      $academic_term = $response_data['Academic term'] . ' ' . $years[0];
    }
    else {
      $academic_term = $response_data['Academic term'] . ' ' . $years[1];
    }

    // Get the row data to be used in theme_table.
    $rows[] = array(
      array(
        'data' => '<a href="entry?id=' . $response_data['Nid'] . '" class="uw-imp-dates-loader">' . $response_data['Title'] . '<div class="important-dates-special-notes">' . $response_data['Special notes'] . '</div></a>',
      ),
      array(
        'data' => $response_data['Description'],
        'class' => 'uw-list-description',
      ),
      array(
        'data' => $academic_term,
      ),
      array(
        'data' => $response_data['Date'],
      ),
    );
  }

  // ISTWCMS-2894.
  // Global variable to have the number of entires.
  // This will determine to show the Export Important Dates button.
  $_uw_important_dates_remote_site_num_of_entries = count($rows);

  // The container for the important dates.
  $form['uw-important-dates'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-important-dates')),
  );

  // If there is an archive link and we are on a non-archive page,
  // add the archive link to the page.
  $uw_archive_link_text = variable_get('uw_archive_link_text');
  $uw_archive_link = variable_get('uw_archive_link');
  if ($uw_archive_link_text && $uw_archive_link && !in_array('archive', $args)) {

    // Wrapper for the archive link.
    $form['uw-important-dates']['uw-archive-link-wrapper'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('uw-archive-link-wrapper'),
      ),
    );

    // The archive link.
    $form['uw-important-dates']['uw-archive-link-wrapper']['uw-archive-link'] = array(
      '#type' => 'markup',
      '#markup' => l(
        $uw_archive_link_text,
        $uw_archive_link,
        array(
          'attributes' => array(
            'class' => array('uw-archive-link'),
          ),
        )
      ),
    );
  }

  // Get the academic year via API.
  $academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $query_parameters['academic_year']);

  // Set acacemic year.
  $academic_year = $academic_year['data'][0]['label'];

  // The current academic year from settings.
  $current_academic_year = variable_get('uw_important_dates_current_academic_year');

  // Get the current academic term from API.
  $current_academic_year = _uw_important_dates_remote_site_get_request('important_dates_academic_year/' . $current_academic_year);

  // Set the current academic year.
  $current_academic_year = $current_academic_year['data'][0]['label'];

  // ISTWCMS-3025.
  // Removing the tabs and filters if not greater than
  // than the current acacemic year from settings.
  // If less than current, set message.
  if ($academic_year >= $current_academic_year) {

    // Get the tabs.
    _uw_important_dates_remote_site_get_tabs($form, $query_parameters, 'list', 'uw-important-dates');

    _uw_important_dates_remote_site_get_list_filters($form, $query_parameters);
  }
  else {

    // Get the archive message.
    _uw_important_dates_remote_site_get_archive_list($form, $query_parameters);
  }

  if (isset($rows)) {

    // The URL for the table header.
    $url_header = '/important-dates/list';

    // Step through each of the orders and set up the headers.
    foreach ($orders as $order) {
      if (!isset($order['data'])) {
        $headers[] = array(
          'data' => l($order['name'], $url_header, array('query' => $order['parameters'], 'attributes' => array('class' => $order['link_class']))),
          'class' => $order['th_class'],
        );
      }
      else {
        $headers[] = array(
          'data' => $order['data'],
          'class' => $order['th_class'],
        );
      }
    }
  }
  else {
    $html = 'No results found.';
  }

  // The container for the list view.
  $form['uw-important-dates-list'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('uw-important-dates-list')),
  );

  // If there are rows, show the table, if not show no results message.
  if (isset($rows) && count($rows) > 0) {

    // The html for the list view.
    $form['uw-important-dates-list']['list'] = array(
      '#theme' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
      '#empty' => 'No results found.',
    );
  }
  else {

    // The html for the list view with no results.
    $form['uw-important-dates-list']['list'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="important-dates-calendar-message">No results found</div>',
    );
  }

  // Get the JSON data.
  $json_response = _uw_important_dates_remote_site_get_request('important_dates_remote_list_count', $count_parameters);

  // Get the total number of rows.
  $total_rows = isset($json_response['data'][0]['count']) ? $json_response['data'][0]['count'] : 0;

  // Get the total number of pages.
  $page_count = ceil($total_rows / 25);

  if ($page_count > 0) {

    // Initialize the pager.
    $current_page = pager_default_initialize($total_rows, 25);

    // The pager to be displayed.
    $form['uw-important-dates-list']['pager'] = array(
      '#theme' => 'pager',
      '#quantity' => $page_count,
      '#parameters' => $query_parameters,
    );
  }

  return $form;
}
