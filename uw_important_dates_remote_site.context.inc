<?php

/**
 * @file
 * uw_important_dates_remote_site.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_important_dates_remote_site_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_imp_dates_archive_metatag';
  $context->description = 'Important dates archive metatag';
  $context->tag = 'Metatag';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '*/archive/*' => '*/archive/*',
      ),
    ),
  );
  $context->reactions = array(
    'metatag_context_reaction' => array(
      'metatags' => array(
        'und' => array(
          'title' => array(
            'value' => '[current-page:page-title] | [site:name]',
            'default' => '[current-page:page-title] | [site:name]',
          ),
          'description' => array(
            'value' => '',
            'default' => '',
          ),
          'abstract' => array(
            'value' => '',
            'default' => '',
          ),
          'keywords' => array(
            'value' => '',
            'default' => '',
          ),
          'robots' => array(
            'value' => array(
              'noindex' => 'noindex',
              'nofollow' => 'nofollow',
              'noarchive' => 'noarchive',
              'index' => 0,
              'follow' => 0,
              'nosnippet' => 0,
              'noodp' => 0,
              'noydir' => 0,
              'noimageindex' => 0,
              'notranslate' => 0,
            ),
            'default' => array(),
          ),
          'news_keywords' => array(
            'value' => '',
            'default' => '',
          ),
          'standout' => array(
            'value' => '',
            'default' => '',
          ),
          'rating' => array(
            'value' => '',
            'default' => '',
          ),
          'referrer' => array(
            'value' => '',
            'default' => '',
          ),
          'generator' => array(
            'value' => '',
            'default' => '',
          ),
          'rights' => array(
            'value' => '',
            'default' => '',
          ),
          'image_src' => array(
            'value' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
            'default' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
          ),
          'canonical' => array(
            'value' => '[current-page:url:absolute]',
            'default' => '[current-page:url:absolute]',
          ),
          'set_cookie' => array(
            'value' => '',
          ),
          'shortlink' => array(
            'value' => '[current-page:url:unaliased]',
            'default' => '[current-page:url:unaliased]',
          ),
          'original-source' => array(
            'value' => '',
            'default' => '',
          ),
          'prev' => array(
            'value' => '',
            'default' => '',
          ),
          'next' => array(
            'value' => '',
            'default' => '',
          ),
          'content-language' => array(
            'value' => '',
            'default' => '',
          ),
          'geo.position' => array(
            'value' => '',
            'default' => '',
          ),
          'geo.placename' => array(
            'value' => '',
            'default' => '',
          ),
          'geo.region' => array(
            'value' => '',
            'default' => '',
          ),
          'icbm' => array(
            'value' => '',
            'default' => '',
          ),
          'refresh' => array(
            'value' => '',
            'default' => '',
          ),
          'revisit-after' => array(
            'value' => '',
            'period' => '',
            'default' => '',
          ),
          'pragma' => array(
            'value' => '',
            'default' => '',
          ),
          'cache-control' => array(
            'value' => '',
            'default' => '',
          ),
          'expires' => array(
            'value' => '',
            'default' => '',
          ),
          'itemtype' => array(
            'value' => 'Article',
            'default' => 'Article',
          ),
          'itemprop:name' => array(
            'value' => '',
          ),
          'itemprop:description' => array(
            'value' => '',
          ),
          'itemprop:image' => array(
            'value' => '',
          ),
          'author' => array(
            'value' => '',
            'default' => '',
          ),
          'publisher' => array(
            'value' => '',
            'default' => '',
          ),
          'og:site_name' => array(
            'value' => '[site:name]',
            'default' => '[site:name]',
          ),
          'og:type' => array(
            'value' => 'article',
            'default' => 'article',
          ),
          'og:url' => array(
            'value' => '[current-page:url:absolute]',
            'default' => '[current-page:url:absolute]',
          ),
          'og:title' => array(
            'value' => '[current-page:page-title] | [site:name]',
            'default' => '[current-page:page-title] | [site:name]',
          ),
          'og:determiner' => array(
            'value' => '',
            'default' => '',
          ),
          'og:description' => array(
            'value' => '',
            'default' => '',
          ),
          'og:updated_time' => array(
            'value' => '',
            'default' => '',
          ),
          'og:see_also' => array(
            'value' => '',
            'default' => '',
          ),
          'og:image' => array(
            'value' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
            'default' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
          ),
          'og:image:url' => array(
            'value' => '',
            'default' => '',
          ),
          'og:image:secure_url' => array(
            'value' => '',
            'default' => '',
          ),
          'og:image:type' => array(
            'value' => '',
            'default' => '',
          ),
          'og:image:width' => array(
            'value' => '',
            'default' => '',
          ),
          'og:image:height' => array(
            'value' => '',
            'default' => '',
          ),
          'og:latitude' => array(
            'value' => '',
            'default' => '',
          ),
          'og:longitude' => array(
            'value' => '',
            'default' => '',
          ),
          'og:street_address' => array(
            'value' => '',
            'default' => '',
          ),
          'og:locality' => array(
            'value' => '',
            'default' => '',
          ),
          'og:region' => array(
            'value' => '',
            'default' => '',
          ),
          'og:postal_code' => array(
            'value' => '',
            'default' => '',
          ),
          'og:country_name' => array(
            'value' => '',
            'default' => '',
          ),
          'og:email' => array(
            'value' => '',
            'default' => '',
          ),
          'og:phone_number' => array(
            'value' => '',
            'default' => '',
          ),
          'og:fax_number' => array(
            'value' => '',
            'default' => '',
          ),
          'og:locale' => array(
            'value' => '',
            'default' => '',
          ),
          'og:locale:alternate' => array(
            'value' => '',
            'default' => '',
          ),
          'article:author' => array(
            'value' => '',
            'default' => '',
          ),
          'article:publisher' => array(
            'value' => '',
            'default' => '',
          ),
          'article:section' => array(
            'value' => '',
            'default' => '',
          ),
          'article:tag' => array(
            'value' => '',
            'default' => '',
          ),
          'article:published_time' => array(
            'value' => '',
            'default' => '',
          ),
          'article:modified_time' => array(
            'value' => '',
            'default' => '',
          ),
          'article:expiration_time' => array(
            'value' => '',
            'default' => '',
          ),
          'profile:first_name' => array(
            'value' => '',
            'default' => '',
          ),
          'profile:last_name' => array(
            'value' => '',
            'default' => '',
          ),
          'profile:username' => array(
            'value' => '',
            'default' => '',
          ),
          'profile:gender' => array(
            'value' => '',
            'default' => '',
          ),
          'og:audio' => array(
            'value' => '',
            'default' => '',
          ),
          'og:audio:secure_url' => array(
            'value' => '',
            'default' => '',
          ),
          'og:audio:type' => array(
            'value' => '',
            'default' => '',
          ),
          'book:author' => array(
            'value' => '',
            'default' => '',
          ),
          'book:isbn' => array(
            'value' => '',
            'default' => '',
          ),
          'book:release_date' => array(
            'value' => '',
            'default' => '',
          ),
          'book:tag' => array(
            'value' => '',
            'default' => '',
          ),
          'og:video:url' => array(
            'value' => '',
            'default' => '',
          ),
          'og:video:secure_url' => array(
            'value' => '',
            'default' => '',
          ),
          'og:video:width' => array(
            'value' => '',
            'default' => '',
          ),
          'og:video:height' => array(
            'value' => '',
            'default' => '',
          ),
          'og:video:type' => array(
            'value' => '',
            'default' => '',
          ),
          'video:actor' => array(
            'value' => '',
            'default' => '',
          ),
          'video:actor:role' => array(
            'value' => '',
            'default' => '',
          ),
          'video:director' => array(
            'value' => '',
            'default' => '',
          ),
          'video:writer' => array(
            'value' => '',
            'default' => '',
          ),
          'video:duration' => array(
            'value' => '',
            'default' => '',
          ),
          'video:release_date' => array(
            'value' => '',
            'default' => '',
          ),
          'video:tag' => array(
            'value' => '',
            'default' => '',
          ),
          'video:series' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:card' => array(
            'value' => 'summary',
            'default' => 'summary',
          ),
          'twitter:site' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:site:id' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:creator' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:creator:id' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:url' => array(
            'value' => '[current-page:url:absolute]',
            'default' => '[current-page:url:absolute]',
          ),
          'twitter:title' => array(
            'value' => '[current-page:page-title] | [site:name]',
            'default' => '[current-page:page-title] | [site:name]',
          ),
          'twitter:description' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:dnt' => array(
            'value' => '',
          ),
          'twitter:image' => array(
            'value' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
            'default' => 'https://uwaterloo.ca/university-of-waterloo-logo-152.png',
          ),
          'twitter:image:width' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image:height' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image:alt' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image0' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image1' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image2' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:image3' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:player' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:player:width' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:player:height' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:player:stream' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:player:stream:content_type' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:country' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:name:iphone' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:id:iphone' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:url:iphone' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:name:ipad' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:id:ipad' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:url:ipad' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:name:googleplay' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:id:googleplay' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:app:url:googleplay' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:label1' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:data1' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:label2' => array(
            'value' => '',
            'default' => '',
          ),
          'twitter:data2' => array(
            'value' => '',
            'default' => '',
          ),
        ),
      ),
      'metatag_admin' => 0,
      'weight' => '0',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Important dates archive metatag');
  t('Metatag');
  $export['uw_imp_dates_archive_metatag'] = $context;

  return $export;
}
