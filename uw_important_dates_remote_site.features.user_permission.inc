<?php

/**
 * @file
 * uw_important_dates_remote_site.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_important_dates_remote_site_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer important dates'.
  $permissions['administer important dates'] = array(
    'name' => 'administer important dates',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_important_dates_remote_site',
  );

  return $permissions;
}
