<?php

/**
 * @file
 * uw_important_dates_remote_site.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_important_dates_remote_site_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_load_progress_elements';
  $strongarm->value = '.uw-imp-dates-loader';
  $export['page_load_progress_elements'] = $strongarm;

  return $export;
}
